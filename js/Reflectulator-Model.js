/**
 * Dominic Canare
 * dom@domstyle.net
 */

/**
 * (Data)Model
 */
Reflectulator.Model = function(){
	this.construct.apply(this, arguments);
};

Reflectulator.Model.prototype.construct = function(){
	this.renderables = [];
	this.pendingRenderable = false;
}

Reflectulator.Model.prototype.addObject = function(obj){
	this.renderables.push(obj);
	Reflectulator.objectList.addObject(obj);
}

Reflectulator.Model.prototype.removeObject = function(obj){
	this.renderables.remove(obj);
	if(obj instanceof Reflectulator.Mirror || obj instanceof Reflectulator.Wall){
		this.recalculateEverything();
	}
}

Reflectulator.Model.prototype.recalculateEverything = function(){
	for(i in this.renderables){
		var obj = this.renderables[i];
		if(obj.calculate){
			obj.calculate();
		}
	}
};
	

/**
 * Renderable
 */
Reflectulator.Object = function(){
	this.construct.apply(this, arguments);
};
Reflectulator.Object.prototype.construct = function(pos){
	if(!pos) pos = [0, 0];
	this.pos = pos.clone();
};
Reflectulator.Object.prototype.construct.toString = function(){
	return "[" + this.pos + "]";
};

/**
 * LineSegment
 */
Reflectulator.LineSegment = function(){
	this.construct.apply(this, arguments);
};
Reflectulator.LineSegment.inherits(Reflectulator.Object);
Reflectulator.LineSegment.prototype.construct = function(pos, end){
	Reflectulator.Object.prototype.construct.call(this, pos);
	if(!end || end.length == 0) end = this.pos.clone();
	this.end = end.clone();
};
Reflectulator.LineSegment.prototype.getDistance = function(){
	return Reflectulator.Math.distance(this.pos[0], this.pos[1], this.end[0], this.end[1]);
};
Reflectulator.LineSegment.prototype.renderTo = function(ctx){	
	ctx.moveTo(this.pos[0], this.pos[1]);
	ctx.lineTo(this.end[0], this.end[1]);
}
Reflectulator.LineSegment.prototype.toString = function(ctx){	
	return "[" + this.pos + "], [" + this.end + "]";
};

/**
 * Wall
 */
Reflectulator.Wall = function(){
	this.construct.apply(this, arguments);
};
Reflectulator.Wall.inherits(Reflectulator.LineSegment);
Reflectulator.Wall.prototype.construct = function(pos, end){
	Reflectulator.LineSegment.prototype.construct.call(this, pos, end);
};
Reflectulator.Wall.prototype.renderTo = function(ctx){
	ctx.strokeStyle = "#000";
	Reflectulator.LineSegment.prototype.renderTo.call(this, ctx)
};
Reflectulator.Wall.prototype.toString = function(){
	return "Wall(" + Reflectulator.LineSegment.prototype.toString.call(this) + ")";
};

/**
 * Marker
 */
Reflectulator.Marker = function(){
	this.construct.apply(this, arguments);
};
Reflectulator.Marker.inherits(Reflectulator.LineSegment);
Reflectulator.Marker.prototype.construct = function(pos, end){
	Reflectulator.LineSegment.prototype.construct.call(this, pos, end);
};
Reflectulator.Marker.prototype.renderTo = function(ctx){
	ctx.strokeStyle = "#0a0";
	Reflectulator.LineSegment.prototype.renderTo.call(this, ctx)
};
Reflectulator.Marker.prototype.toString = function(){
	return "Marker(" + Reflectulator.LineSegment.prototype.toString.call(this) + ")";
};


/**
 * Mirror
 */	
Reflectulator.Mirror = function(){
	this.construct.apply(this, arguments);
};
Reflectulator.Mirror.inherits(Reflectulator.LineSegment);
Reflectulator.Mirror.prototype.construct = function(pos, end){
	Reflectulator.LineSegment.prototype.construct.call(this, pos, end);
};
Reflectulator.Mirror.prototype.renderTo = function(ctx){
	ctx.strokeStyle = "#00f";
	Reflectulator.LineSegment.prototype.renderTo.call(this, ctx);
};
Reflectulator.Mirror.prototype.toString = function(){
	return "Mirror(" + Reflectulator.LineSegment.prototype.toString.call(this) + ")";
};

/**
 * Ray
 */	
Reflectulator.Ray = function(){
	this.construct.apply(this, arguments);
};
Reflectulator.Ray.inherits(Reflectulator.Object);
Reflectulator.Ray.prototype.construct = function(pos, theta){
	Reflectulator.Object.prototype.construct.call(this, pos);
	this.theta = theta;
	this.segments = [];

	this.calculate();
};
Reflectulator.Ray.prototype.renderTo = function(ctx){
	ctx.strokeStyle = "#f00";
	for(var i=0; i<this.segments.length; i++){
		this.segments[i].renderTo(ctx);
	}
};
Reflectulator.Ray.prototype.toString = function(){
	return "Ray([" + this.pos + "], " + this.theta + ")";
};
Reflectulator.Ray.prototype.getDistance = function(){
	var distance = 0;
	for(var i=0; i<this.segments.length; i++){
		distance += this.segments[i].getDistance();
	}
	return distance;
};
Reflectulator.Ray.prototype.calculate = function(){
	var z = 0;
	this.segments = [];

	var nearestIntersection = false;
	var currentPos = [this.pos[0], this.pos[1]];
	var currentAngle = this.theta;
	var lastObj = null;
	lastObj = false;
	var renderables = Reflectulator.model.renderables.slice(0);
	if(Reflectulator.pendingRenderable){
		renderables.push(Reflectulator.pendingRenderable);
	}
	
	do{
		nearestIntersection = false;
		for(var i in renderables){
			var obj = renderables[i];
			// only look for walls and mirrors
			if((obj instanceof Reflectulator.Wall || obj instanceof Reflectulator.Mirror)){
				// skip the last item hit
				if(obj != lastObj){
					var pos = Reflectulator.Math.raySegmentIntersection(currentPos, currentAngle, obj);
					if(pos != false){
						var distance = Reflectulator.Math.distance(currentPos, pos);
						if(!nearestIntersection || distance < nearestIntersection.distance){
							nearestIntersection = {
								pos: pos,
								distance: distance,
								object: obj,
								theta: pos.theta
							};
						}
					}
				}
			}
		}
		if(nearestIntersection){
			lastObj = nearestIntersection.object;
			this.segments.push(
				new Reflectulator.LineSegment(
					currentPos,
					[nearestIntersection.pos[0], nearestIntersection.pos[1]]
				)
			);
			currentPos[0] = nearestIntersection.pos[0];
			currentPos[1] = nearestIntersection.pos[1];
			currentAngle = nearestIntersection.theta;
			if(lastObj instanceof Reflectulator.Wall){
				nearestIntersection = false;
			}
		}
		z++;
	}while(nearestIntersection && z < 100);
};

/**
 * Ray Group
 */	
Reflectulator.RayGroup = function(){
	this.construct.apply(this, arguments);
};
Reflectulator.RayGroup.inherits(Reflectulator.Object);
Reflectulator.RayGroup.prototype.construct = function(pos, theta, lowerBound, upperBound, rayCount){
	Reflectulator.Object.prototype.construct.call(this, pos);
	this.theta = theta;
	this.upperBound = upperBound;
	this.lowerBound = lowerBound;
	this.rayCount = rayCount;
	this.rays = [];

	this.calculate();
};
Reflectulator.RayGroup.prototype.renderTo = function(ctx){
	for(var i=0; i<this.rays.length; i++){
		this.rays[i].renderTo(ctx);
	}
};
Reflectulator.RayGroup.prototype.toString = function(){
	return "RayGroup([" + this.pos + "], " + this.theta + ", " + this.lowerBound + ", " + this.upperBound + ", " + this.rayCount + ")";
};
Reflectulator.RayGroup.prototype.getLeftRayDistance = function(){
	return this.rays[0].getDistance();
};
Reflectulator.RayGroup.prototype.getRightRayDistance = function(){
	return this.rays[this.rays.length - 1].getDistance();
};
Reflectulator.RayGroup.prototype.getLeftRightDistanceDelta = function(){
	return Math.abs(this.getLeftRayDistance() - this.getRightRayDistance());
};
Reflectulator.RayGroup.prototype.getMinRayDistance = function(){
	var min = this.rays[0].getDistance();
	for(var i=1; i<this.rays.length; i++){
		min = Math.min(min, this.rays[i].getDistance());
	}
	return min;
};
Reflectulator.RayGroup.prototype.getMaxRayDistance = function(){
	var max = this.rays[0].getDistance();
	for(var i=1; i<this.rays.length; i++){
		max = Math.max(max, this.rays[i].getDistance());
	}
	return max;
};
Reflectulator.RayGroup.prototype.getMinMaxDistanceDelta = function(){
	return this.getMaxRayDistance() - this.getMinRayDistance();
};
Reflectulator.RayGroup.prototype.getEndToEndDistance = function(){
	var a = this.rays[0].segments.getLast().end;
	var b = this.rays.getLast().segments.getLast().end;
	return Reflectulator.Math.distance(a[0], a[1], b[0], b[1]);
};
Reflectulator.RayGroup.prototype.calculate = function(){
	this.rays = [];

	var stepSize = (this.upperBound - this.lowerBound) / this.rayCount;
	var angle = this.theta - this.upperBound;
	for(var i=0; i<this.rayCount; i++){
		angle += stepSize;
		
		while(angle < 0) angle += Math.PI * 2;
		while(angle > Math.PI * 2) angle -= Math.PI * 2;
		
		this.rays.push(new Reflectulator.Ray(this.pos, angle));
	}
}

