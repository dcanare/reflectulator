/**
 * Dominic Canare
 * dom@domstyle.net
 */

/**
 * Utilities
 */
Object.prototype.inherits = function(classObject){
	this.prototype = new classObject();
	this.prototype.parent = classObject.prototype;
};

Number.prototype.sign = function() {
	if(this > 0) return 1;
	if(this < 0) return -1;
	return 0;
};

Array.prototype.indexOf = function(item){
	for(var i=0; i<this.length; i++){
		if(this[i] == item) return i;
	}
	return -1;
};

Array.prototype.remove = function(item, removeAll){
	for(var i in this){
		if(this[i] == item){
			this.splice(i, 1);
			if(!removeAll) break;
		}
	}
};

Array.prototype.getLast = function(){
	return this[this.length-1];
};

Array.prototype.clone = function(){
	var a = [];
	for(var i in this){
		a[i] = this[i];
	}
	return a;
};



/**
 * Namespace
 */
var Reflectulator = {
	log : function(msg){
		if(this.logElement){
			var el = document.createElement("div");
			el.className = "logMessage";
			el.appendChild(document.createTextNode((new Date()) + " : " + msg));
			
			this.logElement.appendChild(el);
			el.scrollIntoView(false);
		}else{
			alert(log);
		}
	},

	clearLog : function(){
		while(this.logElement && this.logElement.hasChildNodes()){
			this.logElement.removeChild(this.logElement.lastChild);
		}
	},
	
	init : function(canvas, objectListElement, propertiesTable){
		Reflectulator.objectList = new Reflectulator.ObjectList(objectListElement, propertiesTable);
		Reflectulator.ui = new Reflectulator.UI(canvas);
		Reflectulator.model = new Reflectulator.Model();

		var addAndHide = function(obj){
			Reflectulator.model.addObject(obj);
			Reflectulator.objectList.removeObject(obj);
		};
		var max = 9999;
		addAndHide(new Reflectulator.Wall([-max, -max], [max, -max]));
		addAndHide(new Reflectulator.Wall([max, -max], [max, max]));
		addAndHide(new Reflectulator.Wall([-max, max], [max, max]));
		addAndHide(new Reflectulator.Wall([-max, max], [-max, -max]));

		
		Reflectulator.renderLoop(10);
	},
	
	processCmd : function(cmd){
		this.log("Processing command: \n" + cmd + "\n========================");
		try{
			this.currentCommand = new Function(cmd);
			this.log("Compile successful.");
		}catch(exc){
			this.log(exc);
			throw "Compile failed. Check your syntax.";
		}
		try{
			this.currentCommand();
		}catch(exc){
			this.log("Execution failed: " + exc);
			throw exc;
		}
		Reflectulator.model.recalculateEverything();
	},
	
	renderLoop : function(delay){
		Reflectulator.ui.draw(Reflectulator.model.renderables);
		setTimeout("Reflectulator.renderLoop(" + delay + ")", delay);
	},
	
	setMode : function(m){
		if(m){
			Reflectulator.log("Setting mode " + m);
		}
		if(Reflectulator.currentMode){
			Reflectulator.currentMode.abort();
		}
		if(m == "ADD_RAY"){
			Reflectulator.currentMode = Reflectulator.addRayMode;
		}else if(m == "ADD_RAY_GROUP"){
			Reflectulator.currentMode = Reflectulator.addRayGroupMode;
		}else if(m == "ADD_MIRROR"){
			Reflectulator.currentMode = Reflectulator.addMirrorMode;
		}else if(m == "ADD_WALL"){
			Reflectulator.currentMode = Reflectulator.addWallMode;
		}else if(m == "ADD_MARKER"){
			Reflectulator.currentMode = Reflectulator.addMarkerMode;
		}else if(m == "GRAB_START"){
			Reflectulator.currentMode = Reflectulator.grabStartPointMode;
		}else if(m == "GRAB_END"){
			Reflectulator.currentMode = Reflectulator.grabEndPointMode;
		}else if(m == "GRAB_OBJECT"){
			Reflectulator.currentMode = Reflectulator.grabObjectMode;
		}else{
			Reflectulator.log("Disabling mode");
			Reflectulator.currentMode = false;
		}
		
		if(Reflectulator.currentMode && Reflectulator.currentMode.init){
			Reflectulator.currentMode.init();
		}
	},
	
	add : function(str){
		this.log("Creating " + str);
		var obj = eval("new Reflectulator." + str);
		this.model.addObject(obj);
		
		return obj;
	}
};

/*	
	this.Group = function(x, y, theta, omitFromRenderableList){
		this.pos = [x, y];
		this.theta = theta;
		this.items = [];
		
		this.push = function(item){
			Reflectulator.Objects.remove(item);
			this.items.push(item);
		}
		
		this.toString = function(){
			return "Group[" + this.x + ", " + this.y + "]";
		};
		
		this.renderTo = function(ctx){
			for(var i in this.items){
				i.renderTo(ctx);
			}
		}
		
		if(!omitFromRenderableList){
			Reflectulator.Objects.push(this);
		}
	}
	
	
	//~ this.addPendingRenderable = function(){
		//~ Reflectulator.log("new " + Reflectulator.pendingRenderable.toString());
		//~ Reflectulator.Objects.push(Reflectulator.pendingRenderable);
		//~ Reflectulator.draw(true);
		//~ Reflectulator.pendingRenderable = false;
	//~ };

};
*/

Reflectulator.addRayMode = {
	click : function(e, pos){
		if(!this.tmpObject){
			// first click, create a new object
			this.tmpObject = new Reflectulator.Ray(pos, 0);
			Reflectulator.model.addObject(this.tmpObject);
		}else if(this.tmpObject.pos[0] != pos[0] || this.tmpObject.pos[1] != pos[1]){
			// second click, not at the same location
			this.tmpObject = false;
		}else{
			// second click, same location
			this.abort();
		}
	},
	
	mouseMove : function(e, pos){
		if(this.tmpObject){
			var angle = Reflectulator.Math.angleBetweenPoints(
				this.tmpObject.pos[0], this.tmpObject.pos[1],
				pos[0], pos[1]
			);
			this.tmpObject.theta = angle;
			this.tmpObject.calculate();
			
			Reflectulator.objectList.updateLast();
		}
	}, 
	
	abort : function(){
		Reflectulator.model.removeObject(this.tmpObject);
		this.tmpObject = false;
	}
};

Reflectulator.addRayGroupMode = {
	click : function(e, pos){
		if(!this.tmpObject){
			// first click, create a object set
			var lowerBound = prompt("Lower bound angle (in Radians)", -.2);
			var upperBound = prompt("Upper bound angle (in Radians)", .2);
			var stepCount = prompt("Step count", 20);
			this.tmpObject = new Reflectulator.RayGroup(pos, 0, lowerBound, upperBound, stepCount);
			
			Reflectulator.model.addObject(this.tmpObject);
			
		}else if(this.tmpObject.pos[0] != pos[0] || this.tmpObject.pos[1] != pos[1]){
			// second click, not at the same location
			this.tmpObject = false;
		}else{
			// second click, same location
			this.abort();
		}
	},
	
	mouseMove : function(e, pos){
		if(this.tmpObject){
			var angle = Reflectulator.Math.angleBetweenPoints(
				this.tmpObject.pos[0], this.tmpObject.pos[1],
				pos[0], pos[1]
			);
			this.tmpObject.theta = angle;
			this.tmpObject.calculate();
			
			Reflectulator.objectList.updateLast();
		}
	}, 
	
	abort : function(){
		Reflectulator.model.removeObject(this.tmpObject);
		this.tmpObject = false;
	}
};


Reflectulator.addMirrorMode = {
	click : function(e, pos){
		if(!this.tmpObject){
			// first click, create a new object
			this.tmpObject = new Reflectulator.Mirror(pos, pos);
			Reflectulator.model.addObject(this.tmpObject);
		}else if(this.tmpObject.pos[0] != this.tmpObject.end[0] || this.tmpObject.pos[1] != this.tmpObject.end[1]){
			// second click, not at the same location
			Reflectulator.model.recalculateEverything();
			this.tmpObject = false;
		}else{
			// second click, same location
			this.abort();
		}
	},
	
	mouseMove : function(e, pos){
		if(this.tmpObject){
			this.tmpObject.end = pos;
			Reflectulator.model.recalculateEverything();
			Reflectulator.objectList.updateLast();
		}
	},
	
	abort : function(){
		Reflectulator.model.removeObject(this.tmpObject);
		this.tmpObject = false;
	}
};
	
Reflectulator.addWallMode = {
	click : function(e, pos){
		if(!this.tmpObject){
			// first click, create a new object
			this.tmpObject = new Reflectulator.Wall(pos, pos);
			Reflectulator.model.addObject(this.tmpObject);
		}else{
			// second click, delegate
			return Reflectulator.addMirrorMode.click.apply(this, arguments);
		}
	},
	
	mouseMove : function(e, pos){
		if(this.tmpObject){
			this.tmpObject.end = pos;
			Reflectulator.objectList.updateLast();
		}
	},
	
	abort : function(){
		Reflectulator.model.removeObject(this.tmpObject);
		this.tmpObject = false;
	}
};

Reflectulator.addMarkerMode = {
	click : function(e, pos){
		if(!this.tmpObject){
			// first click, create a new object
			this.tmpObject = new Reflectulator.Marker(pos, pos);
			Reflectulator.model.addObject(this.tmpObject);
		}else{
			// second click, delegate
			return Reflectulator.addMirrorMode.click.apply(this, arguments);
		}
	},
	
	mouseMove : function(e, pos){
		// delegate
		return Reflectulator.addMirrorMode.mouseMove.apply(this, arguments);
	},
	
	abort : function(){
		Reflectulator.model.removeObject(this.tmpObject);
		this.tmpObject = false;
	}
};

Reflectulator.grabStartPointMode = {
	init : function(){
		Reflectulator.grabObjectMode.init.apply(this, arguments);
	},
	
	click : function(e, pos){
		return Reflectulator.grabObjectMode.click.apply(this, arguments);
	},
	
	mouseMove : function(e, pos){
		return Reflectulator.grabObjectMode.mouseMove.apply(this, [e, pos, true, false]);
	},
	
	abort : function(){
		return Reflectulator.grabObjectMode.abort.apply(this, [true, false]);
	}
};

Reflectulator.grabEndPointMode = {
	init : function(){
		return Reflectulator.grabObjectMode.init.apply(this, arguments);
	},
	
	click : function(e, pos){
		return Reflectulator.grabObjectMode.click.apply(this, arguments);
	},
	
	mouseMove : function(e, pos){
		return Reflectulator.grabObjectMode.mouseMove.apply(this, [e, pos, false, true]);
	},
	
	abort : function(){
		return Reflectulator.grabObjectMode.abort.apply(this, [false, true]);
	}
};

Reflectulator.grabObjectMode = {
	init : function(){
		// get objects
		this.selectedObjects = Reflectulator.objectList.getSelectedObjects();
		this.startPosition = Reflectulator.ui.mouseCoords.clone();
		this.lastPosition = Reflectulator.ui.mouseCoords.clone();
		this.lastTheta = 0;
		for(var i=0; i<this.selectedObjects.length; i++){
			if(this.selectedObjects[i].theta !== undefined){
				this.selectedObjects[i].tmpTheta = this.selectedObjects[i].theta;
			}
		}
		this.marker = new Reflectulator.Marker(this.startPosition, this.startPosition);
		Reflectulator.model.addObject(this.marker);
		Reflectulator.objectList.removeObject(this.marker);
	},
	
	click : function(e, pos){
		Reflectulator.model.removeObject(this.marker);
		Reflectulator.currentMode = false;
	},
	
	mouseMove : function(e, pos, moveStart, moveEnd){
		if(moveStart == undefined) moveStart = true;
		if(moveEnd == undefined) moveEnd = true;
		this.marker.end = pos;
	
		var delta = [ pos[0] - this.lastPosition[0], pos[1] - this.lastPosition[1] ];
		var currentTheta = Reflectulator.Math.angleBetweenPoints(
			this.startPosition[0], this.startPosition[1],
			pos[0], pos[1]
		);
		var thetaDelta = currentTheta - this.lastTheta;
		
		for(var i=0; i<this.selectedObjects.length; i++){
			var obj = this.selectedObjects[i];
			if(moveStart && obj.pos){
				obj.pos[0] += delta[0];
				obj.pos[1] += delta[1];
			}
			if(moveEnd){
				if(obj.end){
					obj.end[0] += delta[0];
					obj.end[1] += delta[1];
				}else if(obj.theta !== undefined && !moveStart){
					obj.theta += thetaDelta;
				}
			}
		}
		Reflectulator.model.recalculateEverything();
		Reflectulator.objectList.updateSelected();
		this.lastPosition = pos.clone();
		this.lastTheta = currentTheta;
	},
	
	abort : function(moveStart, moveEnd){
		Reflectulator.model.removeObject(this.marker);
		if(moveStart == undefined) moveStart = true;
		if(moveEnd == undefined) moveEnd = true;

		var delta = [ this.startPosition[0] - this.lastPosition[0], this.startPosition[1] - this.lastPosition[1] ];
		for(var i=0; i<this.selectedObjects.length; i++){
			var obj = this.selectedObjects[i];
			if(moveStart && obj.pos){
				obj.pos[0] += delta[0];
				obj.pos[1] += delta[1];
			}
			if(moveEnd){
				if(obj.end){
					obj.end[0] += delta[0];
					obj.end[1] += delta[1];
				}else if(obj.tmpTheta !== undefined){
					obj.theta = obj.tmpTheta;
					obj.tmpTheta = undefined;
				}
			}
		}
		Reflectulator.objectList.updateSelected();
		Reflectulator.model.recalculateEverything();
		Reflectulator.mode = false;
	}
};



/**
 * Object list
 */
Reflectulator.ObjectList = function(listContainer, propertiesTable){
	var self = this;

	this.objectMap = [];
	this.selectedIndicies = [];
	this.propertiesTable = propertiesTable;

	
	this.addObject = function(obj){
		var el = document.createElement("div");
		el.onclick = this.itemClicked;		
		
		var del = document.createElement("span");
		del.appendChild(document.createTextNode("x"));
		del.className = "button";
		del.onclick = function(){
			self.removeObject(obj);
			Reflectulator.model.removeObject(obj);
		};
		
		el.appendChild(del);
		
		var span = document.createElement("span");
		span.appendChild(document.createTextNode(obj.toString()));
		el.appendChild(span);
		el.className = "off";
		listContainer.appendChild(el);

		this.objectMap.push({
			el : el,
			obj : obj
		});
	};
	
	this.updateLast = function(){
		var item = this.objectMap[this.objectMap.length-1];
		item.el.lastChild.innerHTML = item.obj.toString();
	};
	
	this.updateSelected = function(){
		for(var i=0; i<self.selectedIndicies.length; i++){
			var item = self.objectMap[self.selectedIndicies[i]];
			item.el.lastChild.innerHTML = item.obj.toString();
		}
	};
	
	this.removeObject = function(obj){
		for(var i in this.objectMap){
			var record = this.objectMap[i];
			if(record.obj == obj){
				listContainer.removeChild(record.el);
				this.objectMap.splice(i, 1);
				
				this.selectedIndicies.remove(i);
				break;
			}
		}
	};
	
	this.showProperties = function(obj){
		var tbody = this.propertiesTable.tBodies[0];
		while(tbody.hasChildNodes()){
			tbody.removeChild(tbody.lastChild);
		}
		for(var prop in obj){
			var value;
			
			if(prop == "parent") continue;
			if((typeof obj[prop]) == "function"){
				var m = prop.match("^get(.*)");
				if(m){
					try{
						value = obj[prop].apply(obj);
					}catch(exc){
						if(exc.message) exc = exc.message;
						value = "ERROR: " + exc;
					}
					prop = m[1];
					
				}else{
					continue;
				}
			}else{
				value = obj[prop]
			}
			var row = document.createElement("tr");
			var cell = document.createElement("td");
			cell.appendChild(document.createTextNode(prop));
			row.appendChild(cell);
			
			cell = document.createElement("td");
			cell.appendChild(document.createTextNode(value));
			row.appendChild(cell);
			
			tbody.appendChild(row);
		}
	};
	
	this.itemClicked = function(e){
		if(!e.ctrlKey){
			for(var i=0; i<self.selectedIndicies.length; i++){
				self.objectMap[self.selectedIndicies[i]].el.className = "off";
			}
			self.selectedIndicies = [];
		}
		for(var i in self.objectMap){
			if(self.objectMap[i].el == this){
				if(this.className == "on"){
					self.selectedIndicies.splice(i, 1);
					this.className = "off";
				}else{
					self.selectedIndicies.push(i);
					this.className = "on";
				}
				self.showProperties(self.objectMap[i].obj);
				break;
			}
		}
	};
	
	this.removeSelectedItems = function(){
		for(var i=0; i<self.selectedIndicies.length; i++){
			var objectIndex = self.selectedIndicies[i];
			var item = self.objectMap[objectIndex];
			
			listContainer.removeChild(item.el);
			Reflectulator.model.removeObject(item.obj);
		}
		for(var i=0; i<self.selectedIndicies.length; i++){
			var objectIndex = self.selectedIndicies[i];
			self.objectMap.splice(self.selectedIndicies[i], 1);
		}

		self.selectedIndicies = [];
	};
	
	this.getSelectedObjects = function(){
		var objects = new Array();
		for(var i=0; i<self.selectedIndicies.length; i++){
			objects.push(self.objectMap[self.selectedIndicies[i]].obj);
		}
		return objects;
	};
};


	
Reflectulator.Math = {
	angleBetweenPoints : function(x1, y1, x2, y2){
		if(x1 == x2){
			if(y2 > y1){
				return Math.PI / 2;
			}else{
				return 1.5 * Math.PI;
			}
		}
		if(y1 == y2){
			if(x2 > x1){
				return 0;
			}else{
				return Math.PI;
			}
		}
		var angle = Math.atan((y2 - y1) / (x2 - x1));
		// make sure angle is positive
		while(angle < 0){
			angle += Math.PI;
		}
		// if it's greater than 180, reflect it
		if(angle > Math.PI){
			angle = Math.PI - angle;
		}
		
		// now find the correct quadrant
		if(x1 > x2 && y1 > y2){
			angle = Math.PI + angle;
		}else if(x1 < x2 && y1 > y2){
			angle = Math.PI + angle;
		}
		
		return angle;
	},
	
	distance : function(x1, y1, x2, y2){
		if(x2 == undefined){
			x2 = y1[0];
			y2 = y1[1];
			y1 = x1[1];
			x1 = x1[0];
		}
		return Math.sqrt( Math.pow(x2-x1, 2) + Math.pow(y2-y1, 2) );
	},
	
	/**
	* return false if no intersection
	* else return [x, y] of intersection
	*/
	raySegmentIntersection : function(rayPosition, rayAngle, segment){
		var segmentSlope = (segment.end[1] - segment.pos[1]) / (segment.end[0] - segment.pos[0]);
		var raySlope = Math.tan(rayAngle);
		if(segmentSlope == raySlope){
			return false;
		}
		
		// y = mx + b
		// y = m2x + b2
		// mx + b = m2x + b2
		// mx - m2x = b2 - b
		// x(m - m2) = b2 - b
		// x = (b2 - b) / (m - m2)

		var x, y, b, b2;
		var b = segment.pos[1] - segmentSlope * segment.pos[0];
		var b2 = rayPosition[1] - raySlope * rayPosition[0];
		if(segment.end[0] == segment.pos[0] || segmentSlope == Infinity){
			x = segment.pos[0];
			y = raySlope * x + b2;
		}else if(rayAngle == Math.PI/2 || rayAngle == 1.5 * Math.PI || raySlope == Infinity){
			x = rayPosition[0];
			y = segmentSlope * x + b;
		}else{
			x = (b2 - b) / (segmentSlope - raySlope);
			y = segmentSlope * x + b;
		}


		// test for wrong direction
		if(rayAngle == Math.PI/2 || rayAngle == 1.5 * Math.PI || raySlope == Infinity){
			if((y-rayPosition[1]).sign() != Math.sin(rayAngle).sign()){
				return false;
			}
		}else if(rayAngle == 0 || rayAngle == Math.PI){
			if((x-rayPosition[0]).sign() != Math.cos(rayAngle).sign()){
				return false;
			}
		}else{
			if((y-rayPosition[1]).sign() != Math.sin(rayAngle).sign() || (x-rayPosition[0]).sign() != Math.cos(rayAngle).sign()){
				return false;
			}
		}
		if(segment.end[0] - segment.pos[0] > 0){
			// segment goes left-to-right
			if(x < segment.pos[0] || x > segment.end[0]) return false;
		}else{
			// segment goes right-to-left
			if(x > segment.pos[0] || x < segment.end[0]) return false;
		}
		
		if(segment.end[1] == segment.pos[1]){
			// segment is horizontal
		}else if(segment.end[1] - segment.pos[1] > 0){
			if(y < segment.pos[1] || y > segment.end[1]) return false;
		}else{
			if(y > segment.pos[1] || y < segment.end[1]) return false;
		}
		
		var segmentAngle = Math.atan(segmentSlope);
		return {
			0: x,
			1: y,
			theta: 2* segmentAngle - rayAngle
		};
	}
};
