/**
 * Dominic Canare
 * dom@domstyle.net
 */

/**
 * UI
 */
Reflectulator.UI = function(){
	this.construct.apply(this, arguments);
};

Reflectulator.UI.prototype.construct = function(canvas, objectListElement){
	this.objectListElement = objectListElement;
	this.canvas = canvas;
	this.ctx = canvas.getContext('2d');

	this.objectList = [];

	this.mouseCoords = [0, 0];
	this.snapEnabled = false;
	
	var self = this;
	this.canvas.onclick = function(e){
		self.onClick(e);
	};
	
	document.body.onmousemove = function(e){
		self.onMouseMove(e);
	};
	
	document.onkeyup = function(e){
		self.onKeyUp(e);
	};
	
	this.processKeys = true;
};

Reflectulator.UI.prototype.processKeyEvents = function(doThem){
	this.processKeys = doThem;
}


Reflectulator.UI.prototype.addObject = function(obj){
	this.objectList[el] = obj;
};

Reflectulator.UI.prototype.draw = function(renderables){
	this.clearScreen();
	if(!this.imageBuffer){
		Reflectulator.log("drawing from scratch");
		this.clearScreen();
		this.drawGrid();
		//~ this.drawRenderables(renderables);
		// grab the buffer
		this.imageBuffer = this.ctx.getImageData(0, 0, this.canvas.width, this.canvas.height);
	}else{
		this.ctx.putImageData(this.imageBuffer, 0, 0);
	}
	//~ 
	//~ if(this.pendingRenderable){
		//~ this.ctx.beginPath();
		//~ this.ctx.closePath();
		//~ this.pendingRenderable.renderTo(this.ctx);
		//~ this.ctx.stroke();
	//~ }
	
	//~ if(saveState){
		//~ this.imageBuffer = this.ctx.getImageData(0, 0, canvas.width, canvas.height);
	//~ }

	this.drawRenderables(renderables);

	//~ try{
		//~ this.ctx.beginPath();
		//~ this.ctx.fillStyle    = '#00f';
		//~ this.ctx.font         = '14px monospace';
		//~ this.ctx.textBaseline = 'top';
		//~ this.ctx.fillText  (+(new Date()) + " " + this.mouseCoords, 0, 0);
		//~ this.ctx.closePath();
	//~ }catch(exc){}
	

	this.ctx.beginPath();
	this.ctx.strokeStyle = "#E17E00";
	var tmpLineWidth = this.ctx.lineWidth;
	this.ctx.lineWidth = 2;
	this.ctx.arc(this.mouseCoords[0], this.mouseCoords[1], 5, 0, Math.PI*2, true);
	this.ctx.closePath();
	this.ctx.stroke();

	this.ctx.beginPath();
	this.ctx.fillStyle = "#000000";
	this.ctx.fillRect(this.mouseCoords[0], this.mouseCoords[1], 1, 1);
	this.ctx.fillRect(this.mouseCoords[0]-1, this.mouseCoords[1]-1, 1, 1);
	this.ctx.closePath();
	this.ctx.stroke();

	this.ctx.lineWidth = tmpLineWidth;
	
};

Reflectulator.UI.prototype.drawGrid = function(){
	this.ctx.beginPath();
	this.ctx.strokeStyle = "#ccc";
	for(var x=0; x<=this.canvas.width; x += 10){
		this.ctx.moveTo(x, 0);
		this.ctx.lineTo(x, this.canvas.height);
	}
	for(var y=0; y<=this.canvas.height; y+= 10){
		this.ctx.moveTo(0, y);
		this.ctx.lineTo(this.canvas.width, y);
	}
	this.ctx.closePath();
	this.ctx.stroke();
};

Reflectulator.UI.prototype.clearScreen = function(){
	this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
};

Reflectulator.UI.prototype.drawRenderables = function(renderables){
	for(i in renderables){
		if(!renderables[i].renderTo) continue;
		this.ctx.beginPath();

		renderables[i].renderTo(this.ctx);

		this.ctx.closePath();
		this.ctx.stroke();
	}
};

//~ Reflectulator.UI.prototype.renderLoop = function(delay){
	//~ this.draw();
//~ };

Reflectulator.UI.prototype.setSnap = function(enabled){
	this.snapEnabled = enabled;
}
	
Reflectulator.UI.prototype.onClick = function(e){
	var pos = this.getRelativePosition(e);
	if(Reflectulator.currentMode){
		Reflectulator.currentMode.click(e, pos);
	}
};
	
Reflectulator.UI.prototype.onMouseMove = function(e){
	this.mouseCoords = this.getRelativePosition(e);
	window.status = this.mouseCoords;

	if(Reflectulator.currentMode){
		Reflectulator.currentMode.mouseMove(e, this.mouseCoords);
	}
};

Reflectulator.UI.prototype.onKeyUp = function(e){
	if(!this.processKeys) return;
	switch(e.keyCode){
		case 27:
			Reflectulator.setMode();
		break;
		case 83: // s - grab the start point
			Reflectulator.setMode("GRAB_START");
		break;
		case 69: // e - grab the end point
			Reflectulator.setMode("GRAB_END");
		break;
		case 71: // g - grab the whole thing
			Reflectulator.setMode("GRAB_OBJECT");
		break;
		default:
			Reflectulator.log(e.keyCode);
		break;
	}
};

	
Reflectulator.UI.prototype.findPos = function(obj){
	var curleft = curtop = 0;
	do {
		curleft += obj.offsetLeft;
		curtop += obj.offsetTop;
	} while (obj = obj.offsetParent);
	return [curleft,curtop];
};

Reflectulator.UI.prototype.getRelativePosition = function(x, y){
	var e;
	if(x instanceof MouseEvent){
		e = x;
		y = e.clientY;
		x = e.clientX;
	}

	var pos = this.findPos(this.canvas);
	pos[0] = x - pos[0];
	pos[1] = y - pos[1];
	
	if(this.snapEnabled || (e && e.ctrlKey)){
		pos[0] -= pos[0] % 10;
		pos[1] -= pos[1] % 10;
	}

	return pos;
};
