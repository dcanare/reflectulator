// Retroreflector
var space = 10;
this.model.addObject(new this.Mirror([0,620 + space], [720,620 + space]));
for(var x=0; x<720; x+=space*2){
	this.model.addObject(new this.Mirror([x-1,620 + space +1], [x+space,620]));
	this.model.addObject(new this.Mirror([x+space,620], [x+space*2+1,620 + space + 1]));
}

// Car
this.model.addObject(new this.Marker([300,20], [300,200]));
this.model.addObject(new this.Marker([300,200], [400,200]));
this.model.addObject(new this.Wall([400,200], [400,20]));
this.model.addObject(new this.Wall([400,20], [300,20]));
this.model.addObject(new this.Wall([300,20], [300,200]));
this.model.addObject(new this.Marker([310,160], [390,160]));
this.model.addObject(new this.Marker([390,160], [390,120]));
this.model.addObject(new this.Marker([390,120], [310,120]));
this.model.addObject(new this.Marker([310,40], [310,70]));
this.model.addObject(new this.Marker([310,70], [390,70]));
this.model.addObject(new this.Marker([390,70], [390,40]));
this.model.addObject(new this.Marker([390,40], [310,40]));
this.model.addObject(new this.Marker([370,200], [380,190]));
this.model.addObject(new this.Marker([380,190], [390,200]));
this.model.addObject(new this.Marker([310,200], [320,190]));
this.model.addObject(new this.Marker([320,190], [330,200]));
this.model.addObject(new this.Marker([310,120], [310,160]));

// Driver
this.model.addObject(new this.Wall([380,140], [380,130]));
this.model.addObject(new this.Wall([380,130], [360,130]));
this.model.addObject(new this.Wall([360,140], [360,130]));
this.model.addObject(new this.Wall([360,140], [370,150]));
this.model.addObject(new this.Wall([370,150], [380,140]));
// Passenger
this.model.addObject(new this.Wall([320,140], [320,130]));
this.model.addObject(new this.Wall([320,130], [340,130]));
this.model.addObject(new this.Wall([340,140], [340,130]));
this.model.addObject(new this.Wall([340,140], [330,150]));
this.model.addObject(new this.Wall([320,140], [330,150]));

// headlights
this.model.addObject(new this.RayGroup([380,200], 1.5707963267948966, -.15, .15, 15));
this.model.addObject(new this.RayGroup([320,200], 1.5707963267948966, -.15, .15, 15));

// Car 2
this.model.addObject(new this.Wall([110,380], [30,220]));
this.model.addObject(new this.Wall([30,220], [130,170]));
this.model.addObject(new this.Wall([130,170], [210,330]));
this.model.addObject(new this.Marker([210,330], [110,380]));
this.model.addObject(new this.Marker([118,375], [121,365]));
this.model.addObject(new this.Marker([121,365], [133,367]));
this.model.addObject(new this.Marker([200,335], [186,335]));
this.model.addObject(new this.Marker([186,335], [185,342]));
this.model.addObject(new this.Marker([60,240], [120,210]));
this.model.addObject(new this.Marker([120,210], [130,230]));
this.model.addObject(new this.Marker([60,240], [73,254]));
this.model.addObject(new this.Marker([73,254], [128,230]));
this.model.addObject(new this.Marker([105,345], [185,304]));
this.model.addObject(new this.Marker([184,303], [163,269]));
this.model.addObject(new this.Marker([163,269], [84,309]));
this.model.addObject(new this.Marker([85,309], [103,346]));
this.model.addObject(new this.Wall([159,287], [140,295]));
this.model.addObject(new this.Wall([141,295], [146,305]));
this.model.addObject(new this.Wall([146,305], [160,305]));
this.model.addObject(new this.Wall([160,304], [164,295]));
this.model.addObject(new this.Wall([164,295], [159,287]));
this.model.addObject(new this.Wall([119,307], [101,318]));
this.model.addObject(new this.Wall([101,318], [108,328]));
this.model.addObject(new this.Wall([107,327], [120,328]));
this.model.addObject(new this.Wall([120,327], [125,317]));
this.model.addObject(new this.Wall([125,317], [118,307]));

// siren
var siren = new this.RayGroup([380,200], 1.5707963267948966, -.15, .15, 15);
this.model.addObject(siren);
var rotate = function(){
	siren.theta += 0.075;
	while(siren.theta > Math.PI * 2){
		siren.theta -= Math.PI * 2;
	}
	siren.calculate();
};
setInterval(rotate, 20);

//spinning mirror
var spinrror = new this.Mirror([300,200], [400, 200]);
spinrror.center = [(spinrror.pos[0]+spinrror.end[0])/2, (spinrror.pos[1]+spinrror.end[1])/2]
spinrror.radius = this.Math.distance(spinrror.pos, spinrror.end) / 2;
spinrror.theta = 0;
this.model.addObject(spinrror);

var rotate = function(){
	spinrror.theta += 0.075;
	while(spinrror.theta > Math.PI * 2){
		spinrror.theta -= Math.PI * 2;
	}
	
	spinrror.pos[0] = spinrror.center[0] - Math.cos(spinrror.theta) * spinrror.radius;
	spinrror.pos[1] = spinrror.center[1] + Math.sin(spinrror.theta) * spinrror.radius;
	spinrror.end[0] = spinrror.center[0] + Math.cos(spinrror.theta) * spinrror.radius;
	spinrror.end[1] = spinrror.center[1] - Math.sin(spinrror.theta) * spinrror.radius;
	Reflectulator.model.recalculateEverything();
};
setInterval(rotate, 20);

// bucket lamps
this.model.addObject(new this.RayGroup([302,166], 1.2654377467062596, -0.2, 0.2, 20));
this.model.addObject(new this.RayGroup([380,151], 1.4056476493802699, -0.2, 0.2, 20));
this.model.addObject(new this.Wall([299,160], [311,298]));
this.model.addObject(new this.Wall([382,142], [429,261]));
this.model.addObject(new this.Wall([297,161], [385,143]));
